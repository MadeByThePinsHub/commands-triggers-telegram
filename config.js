// @ts-check
'use strict';

/**
 * Millisecond
 * String to be parsed by https://npmjs.com/millisecond,
 * or number of milliseconds. Pass 0 to remove immediately.
 * @typedef {( number | string )} ms
 */

const config = {

	/**
	 * @type {!( number | string | (number|string)[] )}
	 * ID (number) or username (string) of the bot master,
	 * the person who can promote and demote sudoers,
	 */
	master: 123456789,
  
  /**
	 * @type {!string}
	 * Telegram Bot token obtained from BotFather in Telegram.
   *
   * It's best advise to pull the token from environment variables.
	 */
	token: process.env.TELEGRAM_BOT_TOKEN,
  
  /**
	 * @type {(ms | false)} Millisecond
	 * Timeout before removing join and leave messages.
	 * [Look at typedef above for details.]
	 * Pass false to disable this feature.
   *
   * Defaults to 2 minutes to limit API calls as possible.
	 */
	deleteJoinsAfter: '2 minutes',
 
  /**
	 * @type {string[]}
	 * List of blacklisted domains.
	 * Messages containing blacklisted domains will automatically be warned.
	 * If the link is shortened, an attempt will be made to resolve it.
	 * If resolved link is blacklisted, the bot will delete it.
	 */
	blacklistedDomains: [],
}