#!/bin/sh

echo 'Removing incorrectly linked modules...'
rm -rf /rbd/pnpm-volume/app/node_modules

echo 'Refreshing your Glitch project...'
refresh

echo 'Please wait for up to 90 seconds for pnpm to install Yarn, the sole root dependency.'
echo 'After 90 seconds, run \`${bold}npm run relink-modules${normal}\ in an new terminal session.'
exit