'use strict';

// Utils
const { strip } = require('../utils/parse');

const Datastore = require('nedb-promise');
const R = require('ramda');

const User = new Datastore({
	autoload: true,
	filename: '.data/User.db'
});

User.ensureIndex({
	fieldName: 'id',
	unique: true
});

User.ensureIndex({
	fieldName: 'status',
});

// Migration
User.update(
	{ username: '' },
	{ $unset: { username: true } },
	{ multi: true }
).then(() =>
	User.ensureIndex({ fieldName: 'username', sparse: true, unique: true }));

const normalizeTgUser = R.pipe(
	R.pick([ 'first_name', 'id', 'last_name', 'username' ]),
	R.evolve({ username: R.toLower }),
	R.merge({ first_name: '', last_name: '' }),
);

const getUpdatedDocument = R.prop(1);

const getUser = user =>
	User.findOne(user);

const updateUser = async (rawTgUser) => {
	const tgUser = normalizeTgUser(rawTgUser);

	const { id, username } = tgUser;

	const [ rawDbUser ] = await Promise.all([
		getUser({ id }),
		User.update({ $not: { id }, username }, { $unset: { username: true } }),
	]);

	if (rawDbUser === null) {
		return User.update(
			{ id },
			{ status: 'member', ...tgUser },
			{ returnUpdatedDocs: true, upsert: true }
		).then(getUpdatedDocument);
	}

	const dbUser = rawDbUser;

	if (!R.whereEq(tgUser, dbUser)) {
		return User.update(
			{ id },
			{ $set: tgUser },
			{ returnUpdatedDocs: true }
		).then(getUpdatedDocument);
	}

	return dbUser;
};

const admin = ({ id }) =>
	User.update(
		{ id },
		{ $set: { status: 'sudo', warns: [] } }
	);

const getAdmins = () =>
	User.find({ status: 'sudo' });

const removeSudoRights = ({ id }) =>
	User.update({ id }, { $set: { status: 'user' } });

const isSudoer = (user) => {
	if (!user) return false;

	if (user.status) return user.status === 'sudo';

	return User.findOne({ id: user.id, status: 'sudo' });
};

const gban = ({ id }, ban_details) =>
	User.update(
		{ id, $not: { status: 'sudo' } },
		{ $set: { ban_details, status: 'gbanned' } },
		{ upsert: true }
	);

const batchGban = (users, ban_details) =>
	User.update(
		{ $or: users.map(strip), $not: { status: 'sudo' } },
		{ $set: { ban_details, status: 'gbanned' } },
		{ multi: true, returnUpdatedDocs: true }
	).then(getUpdatedDocument);

const ensureExists = ({ id }) =>
	id && User.insert({ id, status: 'user', warns: [] }).catch(R.F);

const ungban = ({ id }) =>
	User.update(
		{ id },
		{
			$set: { status: 'user' },
			$unset: { ban_details: true, ban_reason: true },
		}
	);


module.exports = {
	admin,
	gban,
	batchGban,
	ensureExists,
	getAdmins,
	getUser,
	isSudoer,
	removeSudoRights,
	ungban,
	updateUser,
};