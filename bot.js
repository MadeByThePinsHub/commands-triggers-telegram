const express = require("express");
const app = express();

const Telegraf = require("telegraf");
const Markup = require("telegraf/markup");
const Extra = require("telegraf/extra");

const options = {
  telegram: {
    webhookReply: false
  }
};
const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN, options);

app.use(bot.webhookCallback("/api/webhookEndpoint"));
bot.telegram.setWebhook(process.env.BASE_URL + "/api/webhookEndpoint");

// Check ping times!
bot.use(async (ctx, next) => {
  let start = new Date();
  await next();
  let ms = new Date() - start;
  if ((ms = "0ms")) {
    console.warn("Response time was 0ms. Check the code!");
  } else {
    console.log("Response time: %sms", ms);
  }
});

// Handle unmatched command stuff, etc.
bot.use(require("./handlers/unmatched"));

const keyboardOnStart = Markup.inlineKeyboard([
  Markup.urlButton(
    "Remix me on Glitch!",
    "https://glitch.com/edit/#!/remix/upbeat-amazing-collar"
  )
]);

// Greet our users with a simple hello
bot.start(ctx =>
  ctx.replyWithMarkdownV2(
    "*Hello, world\\!*" +
      "\n\n" +
      "I am just a bot made by the Pins team for creating custom commands with code written on the server\\-side\\." +
      "\n\n" +
      "To learn more, see /help\\.",
    Extra.markup(keyboardOnStart)
  )
);

// Help
bot.help(ctx => {
  ctx.replyWithMarkdownV2(
    "*Help*" +
      "\n\n" +
      "For the available list of triggers, use /list\\_triggers\\." +
      "\n" +
      "For support using the bot, use /support\\."
  );
});

bot.catch((err, ctx) => {
  console.log(`Ooops, encountered an error for ${ctx.updateType}`, err);
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});

const { execSync } = require("child_process");

app.post("/api/deploy/merge", (request, response) => {
  if (request.query.secret !== process.env.SECRET) {
    response
      .status(401)
      .send({
        webhookReceied: true,
        ok: false,
        description:
          "The 'secret' request query didn't matched with ours. Try again later."
      });
    return;
  }

  if (request.body.ref !== "refs/heads/master") {
    response
      .status(200)
      .send({
        webhookReceived: true,
        ok: false,
        description: "Push was not to the master branch, so it did not deploy."
      });
    return;
  }

  const repoUrl = request.body.repository.git_url;

  console.warn("Fetching latest changes from Git repository...");
  const output = execSync(
    `git checkout -- ./ && git pull -X theirs ${repoUrl} master && refresh`
  ).toString();
  console.log(output);
  response
    .status(200)
    .send({ webhookReceived: true, ok: true, description: "Success!" });
});

app.get("/api/deploy", (req, res) => {
  res
    .status(405)
    .send({
      webhookReceived: false,
      ok: false,
      description: "GET method unsupported. Try again with POST."
    });
});

app.get("api/glitch/reload", (req, res) => {
  res.status(200).send({ status: 200, description: "Reloading..." });
  const output = execSync(`/usr/bin/refresh`);
});

app.get("/api/git-status", (req, res) => {
  const output = execSync(`git status`);
  res
    .status(200)
    .send({
      ok: true,
      status: 200,
      decription: "Git status loaded.",
      output: output
    });
});

app.listen(3000, () => {
  console.log("Your app is listening on port 3000!");
});
