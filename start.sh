maintainanceModeError() {
  echo "You enabled the maintainance mode, exiting..."
  echo "To go back to business, make 'MAINTAINANCE_MODE_STATUS' environment variable to 'disabed' or 'false'."
  exit 1
}


if [ $MAINTAINANCE_MODE_STATUS == "enabled" ]; then
  maintainanceModeError
elif [ $MAINTAINANCE_MODE_STATUS == 'true' ]; then
  maintainanceModeError
else
  echo 'Continuning the boot process...'
fi