# Setting Up on Your Own Server

## With Glitch
### Deploy from your fork
This is the recommended option if you want to deploy your remixed code from your Git repo to Glitch. The follow

1. [Fork the app from the GitLab mirror.](https://gitlab.com/MadeByThePinsTeam-DevLabs/commands-triggers-telegram/-/forks/new)
2. Launch Glitch.com and sign in. In the online editor (`glitch.com/edit`), click your project slug -> **New Project** -> **Clone from Git repo**. Paste the Git repo of your fork.
3. Open a new terminal session in **Tools** -> **Full Screen Terminal** and do some code.
    - Run `npm run create-env:from-template` to copy `.env.example` and edit the newly-copied `.env` file.
    - Create a hook with `npm run git-glitched:CreatePost-receiveHooks`.
    - Generate SSH keys, and use it as Deploy Keys in your project's CI/CD settings.
4. 

## Locally Running
1. Run `git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/commands-triggers-telegram.git` on your home directory.
2. Open the directory by changing the working directory with `cd commands-triggers-telegram`.
3. Run `npm run create-env:from-template` to automate the creation of `.env` file from

## Wer the docs?
<https://gitlab.com/MadeByThePinsTeam-DevLabs/commands-triggers-telegram/-/wikis/home>