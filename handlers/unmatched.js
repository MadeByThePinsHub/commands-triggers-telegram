'use strict';

/** @param { import('../typings/context').ExtendedContext } ctx */
const unmatchedHandler = ctx => {
	ctx.state[unmatchedHandler.unmatched] = true;
	if (ctx.chat && ctx.chat.type === 'private') {
		ctx.reply('Sorry, I couldn\'t understand the reference Do you need any /help?');
	} else {
    ctx.reply('Stop yeeting commands, please.')
  }
};

unmatchedHandler.unmatched = Symbol('unmatchedHandler.unmatched');

module.exports = unmatchedHandler;